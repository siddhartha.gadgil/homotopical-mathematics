---
title: Motivation for infinity-structures
---

### Loop spaces

These are characterized by admitting $`A_\infty`$-structures.

### Homotopy equivalences and chain homotopies

A homotopy equivalence does not transport, for example, an associative product to an associative product. Instead what we get is the corresponding $`\infty`$-structure. This is also true in the chain complex case  with quasi-isomorphisms.

### Finite dimensional versions of PDEs (following Sullivan)

Geometric PDEs are associated to the deRham complex, the Hodge star and various  associated bundles. The deRham complex is _quasi-isomorphic_ to simplicial complexes, which have a Hodge-star too. Further, subdivision gives quasi-isomorphisms. So the natural structures to consider corresponding to products of functions and forms are $`\infty`$-structures.

### Refining transversality (Fukaya, Kontsevich etc)

Many constructions in differential topology are based on choosing a regular value (perhaps by perturbing an equation) and showing independence of choice up to boundaries. However, there is more structure if we do not perturb in this fashion, but consider the inverse image in the appropriate way. This amounts to noting that choices equal up to boundary can be extended infinitely.

### Homotopy type theory

This has many advantages over ZFC.
