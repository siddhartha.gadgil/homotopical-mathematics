import ammonite.ops._
val wd = pwd
val pubd = pwd / "public"
val hello =
"""
<html>
<body>
Hello GitLab
</body>
</html>
"""

val top =
"""
<html>
<head>
<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Homotopical Mathematics </title>

<!-- Latest compiled and minified CSS  for Bootstrap -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <!-- mathjax config similar to math.stackexchange -->
  <script type="text/javascript" async
        src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-MML-AM_CHTML">
      </script>


  <script type="text/x-mathjax-config">
  MathJax.Hub.Config({
  jax: ["input/TeX", "output/HTML-CSS"],
  tex2jax: {
  inlineMath: [ ['$', '$'] ],
  displayMath: [ ['$$', '$$']],
  processEscapes: true,
  skipTags: ['script', 'noscript', 'style', 'textarea', 'pre', 'code']
  },
  messageStyle: "none",
  "HTML-CSS": { preferredFont: "TeX", availableFonts: ["STIX","TeX"] }
  });
  </script>
</head>
<body>
<div class="container">
<p>&nbsp;</p>
<ul class="list-inline">
<li><a href="http://siddhartha.gadgil.gitlab.io/homotopical-mathematics/index.html" class="btn btn-primary" >Home</a></li>
<li><a href="https://gitlab.com/siddhartha.gadgil/homotopical-mathematics/wikis/home" class="btn btn-info" target="_blank">Wiki</a></li>
<li><a href="https://gitlab.com/siddhartha.gadgil/homotopical-mathematics/" class="btn btn-success" target="_blank">Repository</a></li>
<li><a href="https://gitlab.com/siddhartha.gadgil/homotopical-mathematics/issues/" class="btn btn-primary" target="_blank">Issue Tracker</a></li>
</ul>
"""

val foot =
"""
</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</body>
</html>
"""

import $ivy.`com.atlassian.commonmark:commonmark:0.11.0`
import org.commonmark.node._
import org.commonmark.parser.Parser;
import org.commonmark.renderer.html.HtmlRenderer;
import $ivy.`com.lihaoyi::scalatags:0.6.7`
import scalatags.Text.all._

def fromMD(s: String) = {
    val parser = Parser.builder().build()
    val document = parser.parse(s)
    val renderer = HtmlRenderer.builder().build()
    renderer.render(document).replace("$<code>", "$").replace("</code>$", "$")
  }


def threeDash(s: String) = s.trim == "---"

def withTop(l: Vector[String]) = (l.filter(threeDash).size == 2) && threeDash(l.head)

def body(l: Vector[String]) = if (withTop(l)) l.tail.dropWhile((l) => !threeDash(l)).tail else l

def topmatter(lines: Vector[String]) = if (withTop(lines))  Some(lines.tail.takeWhile((l) => !threeDash(l))) else None

def titleOpt(l: Vector[String]) =
  for {
    tm <- topmatter(l)
    ln <- tm.find(_.startsWith("title: "))
    } yield ln.drop(6).trim

def filename(s: String) = s.toLowerCase.replaceAll("\\s", "-")

case class Note(name: String, content: String, optTitle: Option[String]){
  val title = optTitle.getOrElse(name)

  val target = pubd / "notes"/ s"$name.html"

  def url(rel: String) = s"$rel$name.html"

  lazy val output =
    doc(
      content,
      "",
      title)

  def save = write.over(target, output)
}

def getNote(p: Path) =
  {
  val l = read.lines(p)
  val name = titleOpt(l).map(filename).getOrElse(p.name.dropRight(p.ext.length + 1))
  val content = p.ext match {
     case "md" => fromMD(body(l).mkString("\n"))
     case "html" => body(l).mkString("\n")
  }
  Note(name, content, titleOpt(l))
  }

val allNotes = ls.rec(pwd / "_notes").map(getNote)



def notesList(rel: String)  =
  ul(
    allNotes.map(
      (note) =>
        li(
          a(href:=note.url(rel))(note.title)
        )
    ) : _*
  )


def doc(s: String, rel: String, t: String = "") =
s"""
$top<h1 class="text-center">$t</h1>\n
<div class="row">
  <div class="col-md-8 col-sm-12 col-xs-12">
  $s
  </div>
  <div class="col-md-4 col-sm-12 col-xs-12">
  <h3> Notes </h3>

  ${notesList(rel)}
  </div>
</div>
$foot
"""


def readMe = read(wd / "README.md")

lazy val home = doc(fromMD(readMe), "notes/", "Homotopical Mathematics: Some Explorations")

write.over(pubd / "index.html", home)


allNotes.foreach(_.save)
